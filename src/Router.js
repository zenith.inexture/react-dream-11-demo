import React from "react";
import { Route, Routes } from "react-router-dom";
import WK from "./components/WK";
import ALLR from "./components/ALLR";
import BATS from "./components/BATS";
import BWL from "./components/BWL";
import Dream11 from "./components/Dream11";
import CaptainAndViceCaptain from "./components/CaptainAndViceCaptain";
import ViewTeam from "./components/ViewTeam";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Dream11 />}>
        <Route index element={<WK />} />
        <Route path="WK" element={<WK />} />
        <Route path="ALLR" element={<ALLR />} />
        <Route path="BATS" element={<BATS />} />
        <Route path="BWL" element={<BWL />} />
        <Route path="candvc" element={<CaptainAndViceCaptain />} />
      </Route>
      <Route path="view-team" element={<ViewTeam />} />
    </Routes>
  );
};

export default Router;
