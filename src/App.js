import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { fetchPlayersData } from "./redux/playersData/playersAction";
import Router from "./Router";

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchPlayersData());
    // eslint-disable-next-line
  }, []);
  return (
    <div>
      <Router />
    </div>
  );
}

export default App;
