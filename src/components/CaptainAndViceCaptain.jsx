import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import {
  makeCaption,
  makeViceCaption,
  removeCaption,
  removeViceCaption,
} from "../redux/dream11/dream11Action";

const CaptainAndViceCaptain = () => {
  //instance for the location and dispatch method
  const location = useLocation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  //fetched data form the redux store
  const captain = useSelector((state) => state.finalPlayers.captain);
  const viceCaptain = useSelector((state) => state.finalPlayers.viceCaptain);

  /**
   * send the data of 11 player, captain and vice-captain in next page for display that player
   */
  const handleSubmit = () => {
    navigate("/view-team", {
      state: {
        myTeam: location.state,
        captain: captain,
        viceCaptain: viceCaptain,
      },
    });
  };

  return (
    <>
      <div className="row justify-content-md-center">
        <div className="row text-center">
          <div className="col-2">
            <i className="fa-solid fa-shirt" style={{ color: "blue" }}></i>
            <span style={{ marginLeft: "8px" }}>INV</span>
          </div>
          <div className="col-8">
            <div className="alert alert-info validation-msg" role="alert">
              <b>
                You can only select one-one captain and vice captain for your
                team
              </b>
            </div>
          </div>
          <div className="col-2">
            <span style={{ marginRight: "8px" }}>PKC</span>
            <i className="fa-solid fa-shirt" style={{ color: "red" }}></i>
          </div>
        </div>
        <table className="table table-hover text-center">
          <thead>
            <tr className="text-warning">
              <th scope="col">TEAM</th>
              <th scope="col">PLAYER</th>
              <th scope="col">Captain</th>
              <th scope="col">Vice-Captain</th>
            </tr>
          </thead>
          <tbody className="table-body">
            {location?.state?.map((i, index) => (
              <tr key={index}>
                <td>
                  <i
                    className={
                      i.oTeam.sShortName === "INV"
                        ? "fa-solid fa-shirt shirt-b"
                        : "fa-solid fa-shirt shirt-r"
                    }
                  ></i>
                </td>
                <td>{i.sName}</td>
                <td>
                  {JSON.stringify(i) === JSON.stringify(captain) ? (
                    <i
                      className="fa-solid fa-minus player-remove"
                      onClick={() => {
                        dispatch(removeCaption(i));
                      }}
                    ></i>
                  ) : (
                    <i
                      className={
                        JSON.stringify(i) === JSON.stringify(viceCaptain) ||
                        (JSON.stringify(i) !== JSON.stringify(captain) &&
                          captain)
                          ? "fa-solid fa-plus player-add disabled"
                          : "fa-solid fa-plus player-add"
                      }
                      onClick={() => {
                        dispatch(makeCaption(i));
                      }}
                    ></i>
                  )}
                </td>
                <td>
                  {JSON.stringify(i) === JSON.stringify(viceCaptain) ? (
                    <i
                      className="fa-solid fa-minus player-remove"
                      onClick={() => {
                        dispatch(removeViceCaption(i));
                      }}
                    ></i>
                  ) : (
                    <i
                      className={
                        JSON.stringify(i) === JSON.stringify(captain) ||
                        (JSON.stringify(i) !== JSON.stringify(viceCaptain) &&
                          viceCaptain)
                          ? "fa-solid fa-plus player-add disabled"
                          : "fa-solid fa-plus player-add"
                      }
                      onClick={() => {
                        dispatch(makeViceCaption(i));
                      }}
                    ></i>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </table>

        <div className="d-grid">
          <button
            type="button"
            className="btn btn-success text-light"
            onClick={handleSubmit}
            disabled={!captain || !viceCaptain}
          >
            Submit
          </button>
        </div>
      </div>
    </>
  );
};

export default CaptainAndViceCaptain;
