import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import RoleLable from "./RoleLable";
import GroundImage from "../assets/images/cricket-grounds-vector-1094595.jpg";
import { clearFinalValues } from "../redux/finalValues/finalValuesAction";
import { clearDream11Team } from "../redux/dream11/dream11Action";

const ViewTeam = () => {
  //instance created
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  //take data fom the sender route with location
  const captain = location.state.captain;
  const viceCaptain = location.state.viceCaptain;

  //filter other 9 player of the team excepts captain and vice-captain
  const teamExceptCandVC = location.state.myTeam.filter(
    (i) =>
      JSON.stringify(i) !== JSON.stringify(captain) &&
      JSON.stringify(i) !== JSON.stringify(viceCaptain)
  );

  /**
   * navigate to the another page and clear all the data in redux
   */
  const handleSubmit = () => {
    dispatch(clearFinalValues());
    dispatch(clearDream11Team());
    navigate("/WK");
  };

  return (
    <>
      <div className="container">
        <div className="row text-center justify-content-center">
          <div className="col-md-6 perent-div" style={{ padding: "0px" }}>
            <div className="text-center mb-3">
              <span className="team-heading">INV vs PKV</span>
            </div>
            <img src={GroundImage} className="ground" alt="Ground Image"></img>
            <div className="col-12 child-div" style={{ fontSize: "10px" }}>
              <div className="row">
                <div className="col">
                  <div className="player-div float-end text-light">
                    <i
                      style={{ fontSize: "15px" }}
                      className={
                        captain.oTeam.sShortName === "INV"
                          ? "fa-solid fa-shirt shirt-b"
                          : "fa-solid fa-shirt shirt-r"
                      }
                    ></i>
                    <RoleLable as="C" color="lightblue" />
                    <RoleLable as={captain.eRole} color="pink" />
                    <div style={{ marginTop: "5px" }}>{captain.sName}</div>
                  </div>
                </div>
                <div className="col">
                  <div className="player-div float-start text-light">
                    <i
                      style={{ fontSize: "15px" }}
                      className={
                        viceCaptain.oTeam.sShortName === "INV"
                          ? "fa-solid fa-shirt shirt-b"
                          : "fa-solid fa-shirt shirt-r"
                      }
                    ></i>
                    <RoleLable as="VC" color="yellow" />
                    <RoleLable as={viceCaptain.eRole} color="pink" />
                    <div style={{ marginTop: "5px" }}>{viceCaptain.sName}</div>
                  </div>
                </div>
              </div>
              <div className="row" style={{ marginTop: "30px" }}>
                {teamExceptCandVC.map((i, index) => (
                  <div
                    className="col-4"
                    style={{ marginTop: "15px" }}
                    key={index}
                  >
                    <div
                      className={
                        ![1, 4, 7].includes(index)
                          ? [0, 3, 6].includes(index)
                            ? "player-div text-light float-end"
                            : "player-div text-light float-start"
                          : "player-div text-light"
                      }
                    >
                      <i
                        style={{ fontSize: "15px" }}
                        className={
                          i.oTeam.sShortName === "INV"
                            ? "fa-solid fa-shirt shirt-b"
                            : "fa-solid fa-shirt shirt-r"
                        }
                      ></i>
                      <RoleLable as={i.eRole} color="pink" />
                      <div style={{ marginTop: "5px" }}>{i.sName}</div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
            <div className="d-grid">
              <button
                type="button"
                className="btn btn-warning text-light"
                onClick={handleSubmit}
                disabled={!captain || !viceCaptain}
              >
                Choose 11 player again
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViewTeam;
