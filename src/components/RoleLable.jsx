import React from "react";

const RoleLable = (props) => {
  return (
    <div
      className="role-lable"
      style={{ margin: "4px", backgroundColor: `${props.color}` }}
    >
      {props.as}
    </div>
  );
};

export default RoleLable;
