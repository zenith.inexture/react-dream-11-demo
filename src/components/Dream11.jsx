import React from "react";
import { useSelector } from "react-redux";
import { NavLink, Outlet, useNavigate, useLocation } from "react-router-dom";
import "../assets/css/style.css";

const Dream11 = () => {
  //created the instance for the navigation and the loaction
  const nevigate = useNavigate();
  const location = useLocation();

  //useSelector the the fetching the data form the redux store
  const finalArray = useSelector((state) => state.finalPlayers.dream11);
  const numOfPlayers = useSelector((state) => state.finalValues.players);
  const remainingCredit = useSelector((state) => state.finalValues.credit);
  const numberOfINVPlayers = useSelector((state) => state.finalValues.inv);
  const numberOfPKCPlayers = useSelector((state) => state.finalValues.pkc);

  //filter items form the final 11 players array
  const numberOfWK = finalArray.filter((i) => i.eRole === "WK").length;
  const numberOfALLR = finalArray.filter((i) => i.eRole === "ALLR").length;
  const numberOfBATS = finalArray.filter((i) => i.eRole === "BATS").length;
  const numberOfBWL = finalArray.filter((i) => i.eRole === "BWL").length;

  /**
   * submit selected final 11 players in next page with navigation
   */
  const handleSubmit = () => {
    nevigate("candvc", { state: finalArray });
  };

  return (
    <section>
      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col-md-6">
            <div className="text-center mb-3">
              <span className="team-heading">INV vs PKV</span>
            </div>
            {!(location.pathname === "/candvc") ? (
              <>
                <div className="row mb-2 text-center">
                  <div className="col-2">
                    <i
                      className="fa-solid fa-shirt"
                      style={{ color: "blue" }}
                    ></i>
                    <span style={{ marginLeft: "8px" }}>INV</span>
                  </div>
                  <div className="col-8">
                    <div
                      className="alert alert-info validation-msg"
                      role="alert"
                    >
                      <b>Maximum 7 players form a team</b>
                    </div>
                  </div>
                  <div className="col-2">
                    <span style={{ marginRight: "8px" }}>PKC</span>
                    <i
                      className="fa-solid fa-shirt"
                      style={{ color: "red" }}
                    ></i>
                  </div>
                </div>
                <div className="row text-center">
                  <div className="col">
                    <NavLink to="WK">
                      <div className="link">WK</div>
                    </NavLink>
                  </div>
                  <div className="col">
                    <NavLink to="ALLR">
                      <div className="link">ALLR</div>
                    </NavLink>
                  </div>
                  <div className="col">
                    <NavLink to="BATS">
                      <div className="link">BATS</div>
                    </NavLink>
                  </div>
                  <div className="col">
                    <NavLink to="BWL">
                      <div className="link">BWL</div>
                    </NavLink>
                  </div>
                </div>
                <div className="progress mt-4 mb-4">
                  <div
                    className="progress-bar"
                    role="progressbar"
                    style={{ width: `${9.09 * numberOfINVPlayers}%` }}
                    aria-valuenow={numberOfINVPlayers}
                    aria-valuemin="0"
                    aria-valuemax="7"
                  >
                    {numberOfINVPlayers}
                  </div>
                  <div
                    className="progress-bar bg-danger"
                    role="progressbar"
                    style={{
                      width: `${9.09 * numberOfPKCPlayers}%`,
                      backgroundColor: "#0dcaf0",
                    }}
                    aria-valuenow={numberOfPKCPlayers}
                    aria-valuemin="0"
                    aria-valuemax="7"
                  >
                    {numberOfPKCPlayers}
                  </div>
                </div>
                <div className="row text-center final-values">
                  <div className="col text-success">
                    Players {numOfPlayers}/11
                  </div>
                  <div className="col " style={{ color: "blue" }}>
                    INV {numberOfINVPlayers}
                  </div>
                  <div className="col " style={{ color: "red" }}>
                    PKC {numberOfPKCPlayers}
                  </div>
                  <div className="col text-success">
                    Credit {remainingCredit}/100
                  </div>
                </div>
              </>
            ) : null}
            <Outlet />
            <div className="d-grid">
              {!(location.pathname === "/candvc") ? (
                <button
                  type="button"
                  className="btn btn-info text-light"
                  disabled={
                    numOfPlayers === 11 &&
                    remainingCredit >= 0 &&
                    numberOfWK > 0 &&
                    numberOfALLR > 0 &&
                    numberOfBATS > 2 &&
                    numberOfBWL > 2
                      ? false
                      : true
                  }
                  onClick={handleSubmit}
                >
                  Choose your captain & vice-captain
                </button>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Dream11;
