import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addPlayerInList,
  removePlayerInList,
} from "../redux/dream11/dream11Action";
import {
  addPlayer,
  addPlayerRemoveCredit,
  removePlayer,
  removePlayerAddCredit,
} from "../redux/finalValues/finalValuesAction";
import TableHeader from "./TableHeader";

const WK = () => {
  //instatnce of the dispatch method
  const dispatch = useDispatch();

  //useSelctores for the fetching the redux data from the store
  const finalArray = useSelector((state) => state.finalPlayers.dream11);
  const playersData = useSelector((state) => state.players.data);
  const remainingCredit = useSelector((state) => state.finalValues.credit);
  const numberOfINVPlayers = useSelector((state) => state.finalValues.inv);
  const numberOfPKCPlayers = useSelector((state) => state.finalValues.pkc);

  //filter the players array and find the wicket keeper from it
  const wkFilter = playersData?.matchPlayer?.filter((i) => i.eRole === "WK");
  const playerRole = playersData?.aPlayerRole?.find((i) => i.sName === "WK");
  const numberOfWK = finalArray.filter((i) => i.eRole === "WK").length;

  /**
   *
   * @param {get item as a perameter} item
   * dispach the method and add perticular player from the redux store
   */
  const handleAddPlayer = (item) => {
    const key = item.oTeam.sShortName;
    dispatch(addPlayerInList(item));
    dispatch(addPlayer(key));
    dispatch(addPlayerRemoveCredit(item.nFantasyCredit));
  };

  /**
   *
   * @param {get item as a perameter} item
   * dispatch the method and remove perticular item form the redux store
   */
  const handleRemovePlayer = (item) => {
    const key = item.oTeam.sShortName;
    dispatch(removePlayerInList(item));
    dispatch(removePlayer(key));
    dispatch(removePlayerAddCredit(item.nFantasyCredit));
  };

  return (
    <>
      <div
        className={
          numberOfWK > 0
            ? "alert alert-primary validation-msg mt-4"
            : "alert alert-danger validation-msg mt-4"
        }
        role="alert"
      >
        {numberOfWK > 0
          ? `select (${playerRole?.nMin}-${playerRole?.nMax}) WK`
          : "At least one wicket keeper required!!"}
      </div>
      <table className="table table-hover text-center main-table">
        <TableHeader />
        <tbody className="table-body">
          {wkFilter?.map((i, index) => (
            <tr key={index}>
              <td>
                <i
                  className={
                    i.oTeam.sShortName === "INV"
                      ? "fa-solid fa-shirt shirt-b"
                      : "fa-solid fa-shirt shirt-r"
                  }
                ></i>
              </td>
              <td>{i.sName}</td>
              <td>{i.nScoredPoints}</td>
              <td>{i.nFantasyCredit}</td>
              <td>
                {finalArray?.find((j) => j === i) ? (
                  <i
                    className="fa-solid fa-minus player-remove"
                    onClick={() => handleRemovePlayer(i)}
                  ></i>
                ) : (
                  <i
                    className={
                      finalArray.length === 11 ||
                      numberOfWK === 3 ||
                      remainingCredit < i.nFantasyCredit ||
                      (numberOfINVPlayers === 7 &&
                        i.oTeam.sShortName === "INV") ||
                      (numberOfPKCPlayers === 7 && i.oTeam.sShortName === "PKC")
                        ? "fa-solid fa-plus player-add disabled"
                        : "fa-solid fa-plus player-add"
                    }
                    onClick={() => handleAddPlayer(i)}
                  ></i>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default WK;
