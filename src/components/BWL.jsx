import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addPlayerInList,
  removePlayerInList,
} from "../redux/dream11/dream11Action";
import {
  addPlayer,
  addPlayerRemoveCredit,
  removePlayer,
  removePlayerAddCredit,
} from "../redux/finalValues/finalValuesAction";
import TableHeader from "./TableHeader";

const BWL = () => {
  //instance for the dispatch method
  const dispatch = useDispatch();

  //fetched the data form the redux store
  const finalArray = useSelector((state) => state.finalPlayers.dream11);
  const playersData = useSelector((state) => state.players.data);
  const remainingCredit = useSelector((state) => state.finalValues.credit);
  const numberOfINVPlayers = useSelector((state) => state.finalValues.inv);
  const numberOfPKCPlayers = useSelector((state) => state.finalValues.pkc);

  //filter the data from the available data for display it
  const bwlFilter = playersData?.matchPlayer?.filter((i) => i.eRole === "BWL");
  const playerRole = playersData?.aPlayerRole?.find((i) => i.sName === "BWL");

  /**
   *
   * @param {get item as a perameter} item
   * dispach the method and add perticular player from the redux store
   */
  const handleAddPlayer = (item) => {
    const key = item.oTeam.sShortName;
    dispatch(addPlayerInList(item));
    dispatch(addPlayer(key));
    dispatch(addPlayerRemoveCredit(item.nFantasyCredit));
  };

  /**
   *
   * @param {get item as a perameter} item
   * dispach the method and remove perticular player from the redux store
   */
  const handleRemovePlayer = (item) => {
    const key = item.oTeam.sShortName;
    dispatch(removePlayerInList(item));
    dispatch(removePlayer(key));
    dispatch(removePlayerAddCredit(item.nFantasyCredit));
  };
  const numberOfBWL = finalArray.filter((i) => i.eRole === "BWL").length;
  return (
    <>
      <div
        className={
          numberOfBWL > 2
            ? "alert alert-primary validation-msg mt-4"
            : "alert alert-danger validation-msg mt-4"
        }
        role="alert"
      >
        {numberOfBWL > 2
          ? `select (${playerRole?.nMin}-${playerRole?.nMax}) BWL`
          : "At least three bowler required!!"}
      </div>
      <table className="table table-hover text-center mt-3">
        <TableHeader />
        <tbody className="table-body">
          {bwlFilter?.map((i, index) => (
            <tr key={index}>
              <td>
                <i
                  className={
                    i.oTeam.sShortName === "INV"
                      ? "fa-solid fa-shirt shirt-b"
                      : "fa-solid fa-shirt shirt-r"
                  }
                ></i>
              </td>
              <td>{i.sName}</td>
              <td>{i.nScoredPoints}</td>
              <td>{i.nFantasyCredit}</td>
              <td>
                {finalArray?.find((j) => j === i) ? (
                  <i
                    className="fa-solid fa-minus player-remove"
                    onClick={() => handleRemovePlayer(i)}
                  ></i>
                ) : (
                  <i
                    className={
                      finalArray.length === 11 ||
                      numberOfBWL === 5 ||
                      remainingCredit < i.nFantasyCredit ||
                      (numberOfINVPlayers === 7 &&
                        i.oTeam.sShortName === "INV") ||
                      (numberOfPKCPlayers === 7 && i.oTeam.sShortName === "PKC")
                        ? "fa-solid fa-plus player-add disabled"
                        : "fa-solid fa-plus player-add"
                    }
                    onClick={() => handleAddPlayer(i)}
                  ></i>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default BWL;
