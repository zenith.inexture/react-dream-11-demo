import React from "react";

const TableHeader = () => {
  return (
    <thead>
      <tr className="text-warning">
        <th scope="col">TEAM</th>
        <th scope="col">PLAYER</th>
        <th scope="col">POINTS</th>
        <th scope="col">CREDIT</th>
        <th scope="col"></th>
      </tr>
    </thead>
  );
};

export default TableHeader;
