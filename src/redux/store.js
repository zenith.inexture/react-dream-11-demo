import thunk from "redux-thunk";

import {
  legacy_createStore as createStore,
  applyMiddleware,
  combineReducers,
} from "redux";

import playersReducer from "./playersData/playersReducer";
import finalValuesReducer from "./finalValues/finalValuesReducer";
import dream11Reducer from "./dream11/dream11Reducer";

const rootReducer = combineReducers({
  players: playersReducer,
  finalValues: finalValuesReducer,
  finalPlayers: dream11Reducer,
});
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
