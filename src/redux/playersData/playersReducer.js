import {
  FETCH_PLAYERSDATA_REQUEST,
  FETCH_PLAYERSDATA_SUCCESS,
  FETCH_PLAYERSDATA_FAILURE,
} from "./playersType";

//initial state
const initialStateuser = {
  loading: false,
  data: {},
  error: "",
};

const playersReducer = (state = initialStateuser, action) => {
  switch (action.type) {
    case FETCH_PLAYERSDATA_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_PLAYERSDATA_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        error: "",
      };
    case FETCH_PLAYERSDATA_FAILURE:
      return {
        loading: false,
        data: {},
        error: action.payload,
      };
    default:
      return state;
  }
};

export default playersReducer;
