import axios from "axios";

import {
  FETCH_PLAYERSDATA_REQUEST,
  FETCH_PLAYERSDATA_SUCCESS,
  FETCH_PLAYERSDATA_FAILURE,
} from "./playersType";

const fetchPlayersRequest = () => {
  return {
    type: FETCH_PLAYERSDATA_REQUEST,
  };
};
const fetchPlayersSuccess = (users) => {
  return {
    type: FETCH_PLAYERSDATA_SUCCESS,
    payload: users,
  };
};
const fetchPlayersFailure = (error) => {
  return {
    type: FETCH_PLAYERSDATA_FAILURE,
    payload: error,
  };
};

export const fetchPlayersData = () => {
  return function (dispatch) {
    dispatch(fetchPlayersRequest());
    axios
      .get("http://localhost:3005/response")
      .then((response) => {
        dispatch(fetchPlayersSuccess(response.data.data));
      })
      .catch((error) => {
        dispatch(fetchPlayersFailure(error.message));
      });
  };
};
