import {
  ADD_CREDIT,
  ADD_PLAYERS,
  CLEAR_DATA,
  REMOVE_CREDIT,
  REMOVE_PLAYERS,
} from "./finalValuesType";

const initialValues = {
  players: 0,
  inv: 0,
  pkc: 0,
  credit: 100,
};

const finalValuesReducer = (state = initialValues, action) => {
  switch (action.type) {
    case ADD_PLAYERS:
      const addKey = action.payload.toLowerCase();
      return {
        ...state,
        players: state.players + 1,
        [addKey]: state[addKey] + 1,
      };
    case REMOVE_PLAYERS:
      const removeKey = action.payload.toLowerCase();
      return {
        ...state,
        players: state.players - 1,
        [removeKey]: state[removeKey] - 1,
      };
    case ADD_CREDIT:
      return {
        ...state,
        credit: state.credit + action.payload,
      };
    case REMOVE_CREDIT:
      return {
        ...state,
        credit: state.credit - action.payload,
      };
    case CLEAR_DATA:
      return {
        players: 0,
        inv: 0,
        pkc: 0,
        credit: 100,
      };
    default:
      return state;
  }
};
export default finalValuesReducer;
