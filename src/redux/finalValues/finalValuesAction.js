import {
  ADD_CREDIT,
  REMOVE_CREDIT,
  ADD_PLAYERS,
  REMOVE_PLAYERS,
  CLEAR_DATA,
} from "./finalValuesType";

export const addPlayer = (shortName) => {
  return {
    type: ADD_PLAYERS,
    payload: shortName,
  };
};

export const removePlayer = (shortName) => {
  return {
    type: REMOVE_PLAYERS,
    payload: shortName,
  };
};

export const addPlayerRemoveCredit = (credit) => {
  return {
    type: REMOVE_CREDIT,
    payload: credit,
  };
};

export const removePlayerAddCredit = (credit) => {
  return {
    type: ADD_CREDIT,
    payload: credit,
  };
};

export const clearFinalValues = () => {
  return {
    type: CLEAR_DATA,
  };
};
