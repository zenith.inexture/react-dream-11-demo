export const ADD_PLAYERS = "ADD_PLAYERS";
export const REMOVE_PLAYERS = "REMOVE_PLAYERS";

export const ADD_CREDIT = "ADD_CREDIT";
export const REMOVE_CREDIT = "REMOVE_CREDIT";

export const CLEAR_DATA = "CLEAR_DATA";
