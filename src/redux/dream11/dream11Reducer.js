import {
  ADD_PLAYER_IN_LIST,
  CLEAR_DATA,
  MAKE_CAPTAIN,
  MAKE_VICE_CAPTAIN,
  PLAYER_LIST_UPDATE,
  REMOVE_CAPTAIN,
  REMOVE_PLAYER_IN_LIST,
  REMOVE_VICE_CAPTAIN,
} from "./dream11Type";

const initialValues = {
  dream11: [],
  captain: null,
  viceCaptain: null,
};

const dream11Reducer = (state = initialValues, action) => {
  switch (action.type) {
    case PLAYER_LIST_UPDATE:
      return {
        dream11: action.payload,
      };
    case ADD_PLAYER_IN_LIST:
      const arrayForAdd = state.dream11;
      arrayForAdd.push(action.payload);
      return {
        dream11: arrayForAdd,
      };
    case REMOVE_PLAYER_IN_LIST:
      const arrayForRemove = state.dream11.filter((i) => i !== action.payload);
      return {
        dream11: arrayForRemove,
      };
    case MAKE_CAPTAIN:
      return {
        ...state,
        captain: action.payload,
      };
    case REMOVE_CAPTAIN:
      return {
        ...state,
        captain: null,
      };
    case MAKE_VICE_CAPTAIN:
      return {
        ...state,
        viceCaptain: action.payload,
      };
    case REMOVE_VICE_CAPTAIN:
      return {
        ...state,
        viceCaptain: null,
      };
    case CLEAR_DATA:
      return {
        dream11: [],
        captain: null,
        viceCaptain: null,
      };
    default:
      return state;
  }
};
export default dream11Reducer;
