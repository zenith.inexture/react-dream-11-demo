import {
  ADD_PLAYER_IN_LIST,
  CLEAR_DATA,
  MAKE_CAPTAIN,
  MAKE_VICE_CAPTAIN,
  PLAYER_LIST_UPDATE,
  REMOVE_CAPTAIN,
  REMOVE_PLAYER_IN_LIST,
  REMOVE_VICE_CAPTAIN,
} from "./dream11Type";

export const updatePlayesList = (newList) => {
  return {
    type: PLAYER_LIST_UPDATE,
    payload: newList,
  };
};
export const addPlayerInList = (item) => {
  return {
    type: ADD_PLAYER_IN_LIST,
    payload: item,
  };
};

export const removePlayerInList = (item) => {
  return {
    type: REMOVE_PLAYER_IN_LIST,
    payload: item,
  };
};

export const makeCaption = (item) => {
  return {
    type: MAKE_CAPTAIN,
    payload: item,
  };
};

export const removeCaption = (item) => {
  return {
    type: REMOVE_CAPTAIN,
    payload: item,
  };
};

export const makeViceCaption = (item) => {
  return {
    type: MAKE_VICE_CAPTAIN,
    payload: item,
  };
};

export const removeViceCaption = (item) => {
  return {
    type: REMOVE_VICE_CAPTAIN,
    payload: item,
  };
};

export const clearDream11Team = () => {
  return {
    type: CLEAR_DATA,
  };
};
